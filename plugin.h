#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/CLIElementBase/clielementbase.h"

#include "../../Interfaces/Middleware/ipluginlinker.h"

#include "qtdownload.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.PluginLinkerCommand" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();

	// PluginBase interface
private slots:
	void handle(const QStringList& args);

private:
	CLIElement* m_cliElement;
	ReferenceInstancePtr<IPluginLinker> m_pluginLinker;
	QtDownload m_download;
};
