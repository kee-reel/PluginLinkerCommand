#include "qtdownload.h"

#include <QCoreApplication>
#include <QUrl>
#include <QtNetwork/QNetworkRequest>
#include <QFile>
#include <QDebug>

QtDownload::QtDownload() :
	QObject(nullptr)
{
	QObject::connect(&m_manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(downloadFinished(QNetworkReply*)));
}

void QtDownload::downloadFinished(QNetworkReply *data) {
	QFile localFile("downloadedfile");
	if (!localFile.open(QIODevice::WriteOnly))
		return;
	const QByteArray sdata = data->readAll();
	localFile.write(sdata);
	localFile.close();
	qDebug() << "File loaded";
	emit done();
}

void QtDownload::download(const QString& url, const QString& targetPath, const QString& name)
{
	m_targetPath = targetPath;
	m_targetName = name;
	QNetworkRequest request(QUrl::fromEncoded(url.toLocal8Bit()));
	QObject::connect(m_manager.get(request), SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
}

void QtDownload::downloadProgress(qint64 recieved, qint64 total)
{
	qDebug() << "QtDownload::downloadProgress" << recieved << total;
}
