#pragma once

#include <QObject>
#include <QString>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>


class QtDownload : public QObject {
	Q_OBJECT
public:
	QtDownload();

signals:
	void done();

public slots:
	void download(const QString& url, const QString& targetPath, const QString& name);
	void downloadFinished(QNetworkReply* data);
	void downloadProgress(qint64 recieved, qint64 total);

private:
	QNetworkAccessManager m_manager;
	QString m_targetPath;
	QString m_targetName;
};
